﻿
# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 | 172.6.4.1 | 172.6.4.127 | 1 a 126
174.187.55.6/23 | 174.187.55.0 | 172.16.4.127  | 1 a 126
10.0.25.253/18 |10.0.25.11 | 10.0.63.255 | 1 a 254
209.165.201.30/27 |209.165.201.111 | 209.165.201.31 | 1 a 30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.

 Amb /23 tinc 510 hosts. 
Justificació:

32 - 23 = 9 bits per host

29 = 512 combinacions posibles

512 - 2 adreçes reservades = 510 hosts

De /16 a /23 hi han 7 bits per fer combinacions de subxarxes. 27 = 128 

combinacions = 128 subxarxes posibles

La màscara CIDR /23 en format binari és: 

11111111.11111111.11111110.00000000     

i en format decimal es: 255.255.254.0
                            

#####3. Donada la xarxa 10.192.0.0:

a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

RESPOSTA: 32-21=11 són 11 zeros.
RESPOSTA: 22^11= 2048 les combinacions estàn bé.
RESPOSTA: 2048 - 2 = 2046 hosts
RESPOSTA: BINARI: 11111111.11111111.11111000.00000000
          DECIMAL: 255.255.248.0
          IP: 10.192.0.0/21

b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?
RESPOSTA: 32-10 = són 22 zeros
 Binari:11111111.11000000.00000000.00000000
RESPOSTA: 22 - 11 = 11 zeros.
RESPOSTA: Les possibilitats amb 11 xifres són = 2^11 = 2048 de les subxarxes de 1500 disp.






c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.

RESPOSTA: anterior era: 10.192.0.0/21
RESPOSTA:32-23=9 bits = 9 zeros.
RESPOSTA: 2^9 = 512 possibles combinacions acceptades.
RESPOSTA: 512-2 adreçes = 510 hosts i els 510 són mes grans que 500.
RESPOSTA: BINARI: 11111111.11111111.11111110.00000000 
RESPOSTA: DECIMAL: 255.255.254.0
RESPOSTA: IP: 10.192.0.0/23

##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius
a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.

RESPOSTA: Mascara: /20 255.255.240.0 = 20  BINARI:  11111111.11111111.1111 0000.00000000  = 4094 dispositius

b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

RESPOSTA: 2^3 = 8 subxarxes = /20 a /23  255.255.252.0 = 22    
BINARI: 11111111.11111111.111111 00.00000000

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

RESPOSTA:  Total hosts(4080) / (8) num subxarxes = (510)host subxarxa
RESPOSTA: Això vol dir que no és permés 650 dispositius per xarxa.

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.
RESPOSTA:  pasem a /21:
    255.255.248.0 = 21   BINARI: 11111111.11111111.11111 000.00000000
DOS SUBXARXES
 RESPOSTA: i. Xarxa:  10.128.0.0/21    00001010.10000000.00000 000.00000000 
 Broadcast:         10.128.7.255    00001010.10000000.00000 111.11111111
  
PRIMER Host:           10.128.0.1      00001010.10000000.00000 000.00000001
   
ULTIM Host:           10.128.7.254    00001010.10000000.00000 111.11111110
  
Hosts per subxarxa:     2046

  ii. Xarxa:        10.128.8.0/21   00001010.10000000.00001 000.00000000
   
 Broadcast:         10.128.15.255   00001010.10000000.00001 111.11111111
       
PRIMER Host:           10.128.8.1        00001010.10000000.00001 000.00000001
       
ULTIM Host:          10.128.15.254   00001010.10000000.00001 111.11111110
  
 Hosts per subxarxa:     2046


5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.
a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

RESPOSTA: 3 bits, ja que 2^1 es = 2 subxarxes , 2^2= 4 subxarxes , 2^3= 8 subxarxes, per tant agafarem 1+2= 3.


b) Dóna la nova MX, tant en format decimal com en binari.

RESPOSTA: La nova màscara de xarxa seria /15.
Binari: 11111111.11111110.00000000.00000000
Decimal: 255.254.0.0

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
XARXA 1:  172.16.0.0 Binari: 10101100.0001 0000.00000000.00000000 
XARXA 2:  172.16.0.14 Binari: 10101100.0001 0000.00000000.00001110
XARXA 3:  172.16.0.28 Binari: 10101100.0001 0000.00000000.00011100
XARXA 4:  172.16.0.42 Binari: 10101100.0001 0000.00000000.00101010
XARXA 5:  172.16.0.56 Binari: 10101100.0001 0000.00000000.00111000
XARXA 6:  172.16.0.70 Binari: 10101100.0001 0000.00000000.01000110
ii. L'adreça de broadcast extern, en format decimal i en binari
BROADCAST 1: 10101100.00010000.00000000.00001101
BROADCAST 2: 10101100.00010000.00000000.00011011
BROADCAST 3: 10101100.00010000.00000000.00101001
BROADCAST 4: 10101100.00010000.00000000.00110111
BROADCAST 5: 10101100.00010000.00000000.01000101
BROADCAST 6: 10101100.00010000.00000000.10000101
iii. El rang d'IPs per a dispositius
 
 RESPOSTA: TOTS SÓN CLASSE B.

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 | 172.16.0.0  |  172.16.0.13   | Clase B            

Xarxa 2 | 172.16.0.14 |  172.16.0.27   | Clase B
Xarxa 3 | 172.16.0.28 | 172.16.0.41    | Clase B
Xarxa 4 | 172.16.0.42 | 172.16.0.55    | Clase B
 
Xarxa 5 | 172.16.0.56 | 172.16.0.69    | Clase B

Xarxa 6 | 172.16.0.70 | 172.16.0.133   | Clase B


Màscara de subxarxa /15 = 255.254.0.0 i /12 = 255.240.0.0 = 254-240 = 14.

Creem les subxarxes a través del 14= 172.16.0.0 -> 172.16.0.14 ->

172.16.0.28 -> 172.16.0.42 -> 172.16.0.56 -> 172.16.0.70. 

Per el Broadcast = restem 1 -> 172.16.0.14 -> el broadcast de xarxa 1=

172.16.0.13.

d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada. 

RESPOSTA: 2^1= 2 , 2^2= 4 , 2^3= , així succesivament.

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada. 

RESPOSTA: Si tenim 3 bits, haurem de fer la fórmula de= 2^n -2 , és a dir, 2^3 -2 = 6. Podrem tener 6 dispositius.


##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.


a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

RESPOSTA: es necessiten 2 bits i será de /24 a /26.

b) Dóna la nova MX, tant en format decimal com en binari.

RESPOSTA: 255.255.255.192 -> 11111111.11111111.11111111.11000000

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari

RESPOSTA: 192.168.1.0 BINARI: 11000000.10101000.00000001.00000000
RESPOSTA: 192.168.1.64  BINARI: 11000000.10101000.00000001.01000000 
RESPOSTA: 192.168.1.128  BINARI: 11000000.10101000.00000001.10000000
RESPOSTA: 192.168.1.192 BINARI: 11000000.10101000.00000001.11000000 





ii. L'adreça de broadcast extern, en format decimal i en binari

RESPOSTA:192.168.1.63 BINARI: 11000000.10101000.00000001.00111111
RESPOSTA:192.168.1.127 BINARI: 11000000.10101000.00000001.01111111  
RESPOSTA: 192.168.1.191 BINARI: 11000000.10101000.00000001.10111111 
RESPOSTA: 192.168.1.255 BINARI:  11000000.10101000.00000001.11111111 

iii. El rang d'IPs per a dispositius

Xarxa 1 = 192.168.1.1 fins 192.168.1.62
Xarxa 2 = 192.168.1.65 fins 192.168.1.126
Xarxa 3 = 192.168.1.129 fins 192.168.1.190
Xarxa 4 = 192.168.1.193 fins 192.168.1.254


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

 RESPOSTA: No,ja que 2^2 = 4 subxarxes.

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

RESPOSTA: 2^6 == 64 = menys xarxa i menys broadcast = 62 dispositius.
